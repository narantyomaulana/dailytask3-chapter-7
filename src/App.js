import { useState } from "react"
import logo from './logo.svg';
import './App.css';
import './index.css'
function App() {
  const [count, setCount] = useState(0);
  const [student,setStudent] = useState("Click Button !!!");

  return (
    <div className="App">
      <header className="App-header">
        <div className = "container">
          <div className="count mb-5">
          <h4>Total Count</h4>
            <h1>{count}</h1>
            <button className="Plus btn btn-success btn-md ms-4  " onClick={() => setCount(count + 1)}>Plus</button>
            <button className="Mines btn btn-danger btn-md ms-4" onClick={() => setCount(count - 1)}>Mines</button>
            <button className="Reset btn btn-warning btn-md ms-4" onClick={() => setCount(0)}>Reset</button>
          </div> 
          <div className="student mt-4">
            <h2 className="text-center mb-3">Siapa yang Jomblo ???</h2>
            <h4 className="justify-content-center">{student}</h4>
              <button className="btn btn-info btn-md ms-3" onClick={() => setStudent("Chandra")}>Jomblo </button>
              <button className="Reset btn btn-warning btn-md ms-3" onClick={() => setStudent("Click Button !!!")}>Reset</button>
              <button className="btn btn-info btn-md ms-3" onClick={() => setStudent("Fadli")}>Jomblo</button>
          </div>
        </div>
      </header>
    </div>
  );
}

export default App;
